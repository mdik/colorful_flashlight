#####################################
# ColorfulFlashlight by CryptKiddie #
#####################################
# imports
import leds
import urandom
import buttons
from utime import sleep
# vars
TRANS_BLUE = (0,200,255)
TRANS_PINK = (255,160,255)
TRANS_WHITE = (255,220,255)

BI_BLUE = (0,56,168)
BI_PURPLE = (155,79,150)
BI_RED = (214,2,112)

color_concepts=((255,220,200), (255,127,255), (255,127,127), (255,0,127), (255,127,0), (255,255,127), "gay", "bi", "trans")

recolorize_delay=0.1
color_concept=0
led_brightness=1
flashlight=0

# functions for color change
def recolorize(rgb):
    if (rgb == "gay"):
        leds.gay(69)
    elif (rgb == "bi"):
        leds.set(12, BI_RED)
        leds.set(13, BI_RED)
        sleep(recolorize_delay)
        for x in range(0,5):
            leds.set(x, BI_RED)
            sleep(recolorize_delay)
        leds.set(5, BI_PURPLE)
        sleep(recolorize_delay)
        for x in range(6,11):
            leds.set(x, BI_BLUE)
            sleep(recolorize_delay)
        leds.set(11, BI_BLUE)
        leds.set(14, BI_BLUE)
    elif (rgb == "trans"):
        leds.set(11, TRANS_BLUE)
        leds.set(12, TRANS_BLUE)
        leds.set(13, TRANS_BLUE)
        leds.set(14, TRANS_BLUE)
        sleep(recolorize_delay*4)
        leds.set(10, TRANS_BLUE)
        leds.set(0, TRANS_BLUE)
        sleep(recolorize_delay)
        leds.set(9, TRANS_BLUE)
        leds.set(1, TRANS_BLUE)
        sleep(recolorize_delay)
        leds.set(8, TRANS_PINK)
        leds.set(2, TRANS_PINK)
        sleep(recolorize_delay)
        leds.set(7, TRANS_PINK)
        leds.set(3, TRANS_PINK)
        sleep(recolorize_delay)
        leds.set(6, TRANS_WHITE)
        leds.set(5, TRANS_WHITE)
        leds.set(4, TRANS_WHITE)
    else:
        for x in range(0,14):
            leds.set(x,rgb)
            sleep(recolorize_delay)

def select_concept(id):
    recolorize(color_concepts[color_concept])
def inc_color_concept():
    global color_concept
    color_concept = (color_concept + 1) % len(color_concepts)
    select_concept(color_concept)
# functions for brightness change
def dim_all(brightness):
    leds.dim_top(brightness)
    leds.dim_bottom(brightness)
def inc_dim():
    global led_brightness
    led_brightness = (led_brightness + 1) % 9
    dim_all(led_brightness)
    
def toggle_flashlight():
    global flashlight
    if flashlight == 0:
        leds.set_flashlight(1)
        flashlight = 1
    else:
        leds.set_flashlight(0)
        flashlight = 0

# Start of main code (Init LEDs)
select_concept(color_concept)
dim_all(led_brightness)
# Main loop
while (True):
    if (buttons.read(4) == 4):
        toggle_flashlight()
    if (buttons.read(2) == 2):
        inc_dim()
    if (buttons.read(1) == 1):
        inc_color_concept()
    sleep(0.2)

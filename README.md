*Most of this code was written by CryptKiddie and published on https://badge.team/projects/colorfulflashlight.
I added a few functions, but as there was no license given with the original code, I'm not sure what the best way of publishing etc. is...*

This Egg provides you with a dimmable flashlight in changeable colors and an alarm mode
